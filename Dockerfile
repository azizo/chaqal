# Build the Go Binary.
FROM golang:1.13 as build_chaqal-api
ENV CGO_ENABLED 0

RUN mkdir -p /api-service

WORKDIR /api-service
COPY chaqal-4aadc55b7280.json chaqal-4aadc55b7280.json
COPY go.* ./
COPY service service

WORKDIR /api-service/service/
RUN go build

# Run the Go Binary in Alpine.
FROM alpine:3.7

COPY --from=build_chaqal-api /api-service/chaqal-4aadc55b7280.json /app/chaqal-4aadc55b7280.json
COPY --from=build_chaqal-api /api-service/service/service /app/main

ENV GOOGLE_APPLICATION_CREDENTIALS $GOOGLE_APPLICATION_CREDENTIALS
ENV NYT_API_KEY $NYT_API_KEY $GOOGLE_APPLICATION_CREDENTIALS

WORKDIR /app
CMD /app/main
EXPOSE 8080

LABEL org.opencontainers.image.title="Chaqal API" \
      org.opencontainers.image.authors="Aziz Az <aziztux@gmail.com>"