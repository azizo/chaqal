package api

import (
	"context"
	"net/http"
	"strconv"

	"gitlab.com/azizoo/chaqal/service/book"
	"gitlab.com/azizoo/chaqal/service/web"

	"go.opencensus.io/trace"
)

// Book represents the Book API method handler set.
type Book struct {
}

// GetAllBooks returns all books
func (p *Book) GetAllBooks(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Book.List")
	defer span.End()

	data, err := book.GetStoredBooks()
	if err != nil {
		return err
	}
	if len(data) != 0 {
		return web.Respond(ctx, w, data, http.StatusOK)
	}
	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// ReturnReadableBooks returns right books for the user
func (p *Book) ReturnReadableBooks(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Book.List")
	defer span.End()
	timeParam := params["time"]
	amountOfTimeInHour, err := strconv.ParseFloat(timeParam, 64)
	if err != nil {
		return err
	}
	data, err := book.GetFilteredBooks(amountOfTimeInHour)
	if err != nil {
		return err
	}
	if len(data) != 0 {
		return web.Respond(ctx, w, data, http.StatusOK)
	}
	return web.Respond(ctx, w, nil, http.StatusNoContent)
}
