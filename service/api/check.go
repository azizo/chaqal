package api

import (
	"context"
	"net/http"

	"gitlab.com/azizoo/chaqal/service/web"
	"go.opencensus.io/trace"
)

// Check provides support for orchestration health checks.
type Check struct {
}

// Health validates the service is healthy and ready to accept requests.
func (c *Check) Health(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Check.Health")
	defer span.End()

	var health struct {
		Status string `json:"status"`
	}

	health.Status = "ok"
	return web.Respond(ctx, w, health, http.StatusOK)
}
