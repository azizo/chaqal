package api

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/azizoo/chaqal/service/mid"
	"gitlab.com/azizoo/chaqal/service/web"
)

// API constructs an http.Handler with all application routes
func API(shutdown chan os.Signal, log *log.Logger) http.Handler {

	app := web.NewApp(shutdown, mid.Logger(log), mid.Errors(log))

	// Register health check endpoint. This route is not authenticated.
	check := Check{}
	app.Handle("GET", "/v1/health", check.Health)

	b := Book{}
	app.Handle("GET", "/books", b.GetAllBooks)
	app.Handle("GET", "/books/:time", b.ReturnReadableBooks)

	return app
}
