package book

import (
	"context"
	"log"
	"math"
	"os"

	"cloud.google.com/go/firestore"
	"github.com/joho/godotenv"
	"google.golang.org/api/iterator"
)

// GetStoredBooks is to get all stored books
func GetStoredBooks() ([]Book, error) {
	dbClient, ctx := GetDBClient("chaqal")
	defer dbClient.Close()
	var books []Book
	iter := dbClient.Collection("books").Documents(ctx)
	for {
		doc, err := iter.Next()
		var book Book
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		err = doc.DataTo(&book)
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}
	return books, nil
}

// GetFilteredBooks is to get filtered books (right reading time)
func GetFilteredBooks(amountOfTimeInHour float64) ([]Book, error) {
	dbClient, ctx := GetDBClient("chaqal")
	var books []Book

	iter := dbClient.Collection("books").Where("AvgReadingTimePerHour", "<=", amountOfTimeInHour).Documents(ctx)
	for {
		doc, err := iter.Next()
		var book Book

		if err == iterator.Done {
			break
		}
		err = doc.DataTo(&book)
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}
	return books, nil
}

// StoreBestSellers is used to store books
func StoreBestSellers(ctx context.Context, dbClient *firestore.Client, genre string) error {
	defer dbClient.Close()

	godotenv.Load()

	books, err := RetrieveBooksPerGenre(genre, os.Getenv("NYT_API_KEY"))
	if err != nil {
		log.Fatalf("Retrieving best-seller books failed with error %s\n", err)
	}
	for _, book := range books {
		bookWordCount, bookPagesCount, err := RetrieveBookDetails(book.ISBN)
		book.PagesCount = bookPagesCount
		book.WordsCount = bookWordCount
		book.AvgReadingTimePerHour = calculateReadingHours(bookWordCount, 250)
		_, _, err = dbClient.Collection("books").Add(ctx, book)
		if err != nil {
			log.Fatalf("Failed adding alovelace: %v", err)
		}
		if err != nil {
			log.Fatalf("Failed adding word and pages count for a book: %v", err)
		}

	}
	return err
}

func calculateReadingHours(wordCount int32, avgReadWordsPerMin int32) float64 {
	return math.Round(float64(wordCount)/float64(avgReadWordsPerMin)/60*100) / 100
}
