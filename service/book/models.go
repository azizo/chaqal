package book

// NYTBooksResponse is representing the root node returned by NYC Book API response
type NYTBooksResponse struct {
	Results Results `json:"results"`
}

// Results is a JSON node included in the NYT Book API response
type Results struct {
	Books []Book `json:"books"`
}

// Book objects will be returned to the client
type Book struct {
	ISBN                  string `json:"primary_isbn10"`
	Title                 string `json:"title"`
	Author                string `json:"author"`
	PagesCount            int32
	WordsCount            int32
	AvgReadingTimePerHour float64
	Rank                  int
}
