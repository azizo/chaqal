package book

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/tidwall/gjson"
)

// RetrieveBooksPerGenre is used retrieve a list of best seller books chosen by New York Times
func RetrieveBooksPerGenre(genre string, apiKey string) ([]Book, error) {
	var responseData NYTBooksResponse
	url := fmt.Sprintf("https://api.nytimes.com/svc/books/v3/lists/current/%s.json?api-key=%s", genre, apiKey)

	response, err := http.Get(url)
	if err != nil || response.StatusCode != 200 {
		log.Fatalf("The HTTP request failed with error\n")
	}
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(responseBody, &responseData)
	if err != nil {
		log.Fatal(err)
	}
	return responseData.Results.Books, err
}

// RetrieveBookDetails is used to retrieve the number of pages and words per book (by ISBN 10)
func RetrieveBookDetails(isbn string) (int32, int32, error) {
	url := "https://yoga.readinglength.com"
	payload := fmt.Sprintf("[{\"query\": \"query WORDCOUNT_QUERY($isbn: String!) {wordCounts(where: {isbn10: $isbn}) {wordCount}}\",\"variables\": {\"isbn\":\"%[1]s\"}},{\"query\": \"query BOOK_FROM_ISBN_QUERY($isbn: String!) {findBook(isbn10: $isbn) {pageCount }}\",\"variables\":{ \"isbn\": \"%[1]s\"}}]", isbn)

	req, err := http.NewRequest("POST", url, strings.NewReader(payload))

	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{}

	response, err := client.Do(req)

	if err != nil || response.StatusCode != 200 {
		log.Fatalf("The HTTP request failed with error\n")
	}

	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	wordCount := gjson.Get(string(responseBody), "0.data.wordCounts.0.wordCount")
	pagesCount := gjson.Get(string(responseBody), "1.data.findBook.pageCount")

	return int32(wordCount.Int()), int32(pagesCount.Int()), err
}
