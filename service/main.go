package main

import (
	"context"
	"expvar"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/ardanlabs/conf"
	"github.com/pkg/errors"
	"gitlab.com/azizoo/chaqal/service/api"
	"gitlab.com/azizoo/chaqal/service/book"
)

var build = "develop"

func main() {
	if err := run(); err != nil {
		log.Println("error: ", err)
		os.Exit(1)
	}
}

func run() error {
	log := log.New(os.Stdout, "Service: ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	var cfg struct {
		APIHost         string        `conf:"default:0.0.0.0:8080"`
		ShutdownTimeout time.Duration `conf:"default:5s"`
	}

	if err := conf.Parse(os.Args[1:], "BOOKS", &cfg); err != nil {
		// note: returning help instructions
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("BOOKS", &cfg)
			if err != nil {
				return errors.Wrap(err, "generating config usage")
			}
			fmt.Println(usage)
			return nil
		}
		return errors.Wrap(err, "parsing config")
	}

	expvar.NewString("build").Set(build)
	log.Printf("main: application initializing: version %q", build)
	defer log.Println("main: completed")

	out, err := conf.String(&cfg)
	if err != nil {
		return errors.Wrap(err, "generating config for output")
	}
	log.Printf("main: config:\n%v\n", out)

	log.Printf("main: storing inital data")

	dbClient, ctx := book.GetDBClient("chaqal")
	book.DeleteCollection(ctx, dbClient, dbClient.Collection("books"))
	book.StoreBestSellers(ctx, dbClient, "paperback-nonfiction")

	log.Println("main: started initializing API support")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	api := http.Server{
		Addr:    cfg.APIHost,
		Handler: api.API(shutdown, log),
	}

	serverErrors := make(chan error, 1)

	go func() {
		log.Printf("main: API listening on %s", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	select {
	case err := <-serverErrors:
		return errors.Wrap(err, "server error")

	case sig := <-shutdown:
		log.Printf("main: %v: start shutdown", sig)

		ctx, cancel := context.WithTimeout(context.Background(), cfg.ShutdownTimeout)
		defer cancel()

		err := api.Shutdown(ctx)
		if err != nil {
			log.Printf("main: graceful shutdown did not complete in %v : %v", cfg.ShutdownTimeout, err)
			err = api.Close()
		}

		switch {
		case sig == syscall.SIGSTOP:
			return errors.New("integrity issue caused shutdown")
		case err != nil:
			return errors.Wrap(err, "could not stop server gracefully")
		}
	}

	return nil
}
