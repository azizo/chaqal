# CHAQAL Service

Find the right NYT best-seller books to read during your flight!

## What does this service do?

1. Storing and returning best-seller books selected by the New York Times (with additional attributes)

After calling the New York Times Books API to retrieve top best-seller books (per genre), the service calls another API to get the number of pages and words for each previously retrieved book (based on ISBN-10). Once that's done, the average reading time per hour will be calculated for each book and will be added into the book object. Finally, each _complete_ book object will be persisted on a Google Cloud Firestore collection, but now, in a similar structure as below:

```json
{
    "primary_isbn10": "014313356X",
    "title": "NO ONE IS TOO SMALL TO MAKE A DIFFERENCE",
    "author": "Greta Thunberg",
    "PagesCount": 112,
    "WordsCount": 16095,
    "AvgReadingTimePerHour": 1.07,
    "Rank": 2
}
```

1. Filtering books for the user based on the time they have
   
By calling /book/[time], the user can inject the number of hours (in float/double format) to allow the service to find books with the right average reading time. For example, if the user has 2.0 hours of time, the book above won't be included in the result returned to the client.

## Available routes

- /books
- /books/[time]

## Why

To play with Go and learn writing production-level web services with it.

The source this project is based on/inspired from:
https://github.com/ardanlabs/service/wiki